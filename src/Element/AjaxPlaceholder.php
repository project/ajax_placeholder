<?php

namespace Drupal\ajax_placeholder\Element;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides an AJAX placeholder render element.
 *
 * This render element allows for dynamically loading content via an AJAX
 * callback. You can also provide a custom `#markup` value, which will be 
 * displayed while the AJAX request is being processed.
 *
 * Example usage:
 * @code
 * $build['awesome'] = [
 *   '#type' => 'ajax_placeholder',
 *   '#callback' => ['mymodule.service:callback', [$arg1, $arg2]],
 *   '#markup' => t('Custom loading message'), // Optional custom markup
 * ];
 * @endcode
 *
 * @RenderElement("ajax_placeholder")
 */
class AjaxPlaceholder extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#pre_render' => [
        [self::class, 'preRender'],
      ],
    ];
  }

  /**
   * Pre-render callback for AJAX placeholders.
   *
   * @param array $element
   *   The renderable array for the element.
   *
   * @return array
   *   The passed in element with changes made to attributes depending on
   *   context.
   */
  public static function preRender(array $element) {
    // Set the callback data in the tempstore and use a temporary hash to
    // retrieve it.
    $tempstore = \Drupal::service('tempstore.shared')->get('ajax_placeholder');
    $hash = md5(Json::encode($element['#callback']));
    $tempstore->set($hash, [
      'url' => \Drupal::request()->getRequestUri(),
      'callback' => $element['#callback'],
    ]);
    return [
      'container' => [
        '#type' => 'container',
        '#callback' => '',
        '#attributes' => [
          'class' => 'js-ajax-placeholder',
          'data-hash' => $hash,
        ],
        'content' => [
          '#markup' => $element['#markup'] ?? t('Loading...'),
        ],
        '#attached' => [
          'library' => ['ajax_placeholder/ajax'],
        ],
      ],
    ];
  }

}
