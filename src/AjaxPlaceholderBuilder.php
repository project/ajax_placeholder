<?php

namespace Drupal\ajax_placeholder;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerResolverInterface;
use Drupal\Core\Security\DoTrustedCallbackTrait;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\Core\Url;

/**
 * LazyBuilder for AJAX placeholders.
 */
class AjaxPlaceholderBuilder {

  use DoTrustedCallbackTrait;

  /**
   * The shared tempstore factory.
   *
   * @var \Drupal\Core\TempStore\SharedTempStoreFactory
   */
  protected SharedTempStoreFactory $sharedTempstore;

  /**
   * The controller resolver.
   *
   * @var \Drupal\Core\Controller\ControllerResolverInterface
   */
  protected ControllerResolverInterface $controllerResolver;

  /**
   * Constructs a new AjaxPlaceholderBuilder object.
   *
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $shared_tempstore
   *   The shared tempstore factory.
   * @param \Drupal\Core\Controller\ControllerResolverInterface $controller_resolver
   *   The controller resolver.
   */
  public function __construct(SharedTempStoreFactory $shared_tempstore, ControllerResolverInterface $controller_resolver) {
    $this->sharedTempstore = $shared_tempstore;
    $this->controllerResolver = $controller_resolver;
  }

  /**
   * Get an AJAX response for a hash value.
   *
   * @param string $hash
   *   The hash value.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The Ajax response.
   */
  public function ajaxCallback($hash) {
    $tempstore = $this->sharedTempstore->get('ajax_placeholder');
    $data = $tempstore->get($hash);

    // If the data is empty, return an empty response.
    if (empty($data)) {
      // @todo Make the AJAX response cacheable.
      // @see https://www.drupal.org/project/drupal/issues/2701085
      return (new AjaxResponse());
    }

    // Check access to the original request URI and return an empty response if
    // access is not allowed.
    $original_request_uri = $data['url'];
    if (Url::fromUserInput($original_request_uri)->access() === FALSE) {
      // @todo Make the AJAX response cacheable.
      // @see https://www.drupal.org/project/drupal/issues/2701085
      return new AjaxResponse();
    }

    [$callback, $args] = $data['callback'];

    if (is_string($callback)) {
      $double_colon = mb_strpos($callback, '::');
      if (FALSE === $double_colon) {
        $callback = $this->controllerResolver->getControllerFromDefinition($callback);
      }
      elseif ($double_colon > 0) {
        $callback = explode('::', $callback, 2);
      }
    }

    // @todo Make the AJAX response cacheable.
    // @see https://www.drupal.org/project/drupal/issues/2701085
    $response = new AjaxResponse();
    if ($callback) {
      $response->addCommand(new ReplaceCommand('[data-hash="' . $hash . '"]', $this->doTrustedCallback($callback, $args, '')));
    }
    return $response;
  }

}
