INTRODUCTION
------------

The AJAX Placeholder module provides a render element that will defer loading
the content to an AJAX request, similar to how lazy builders work. This can
greatly improve the performance and caching of pages. The main benefit over
lazy builders and Bigpipe, is that the AJAX request will load asynchronously
which will significantly improve the user experience when lots of render
elements are placed on the same page.


USAGE
------------

The module provides a new render element to load content via AJAX:

```
$build['awesome'] = [
  '#type' => 'ajax_placeholder',
  '#callback' => ['mymodule.service:callback', [$arg1, $arg2]],
];
```

The module requires no configuration, and can be used by developers to improve
the performance of pages that show lots of different content that can be hard
to cache (like dashboards).
