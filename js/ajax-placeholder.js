/**
 * @file
 * Replace AJAX placeholders with the rendered content.
 */

(function ($, window, Drupal, drupalSettings) {

  'use strict';

  /**
   * Replace AJAX placeholders with the rendered content.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the feedback form.
   */
  Drupal.behaviors.ajaxPlaceholder = {
    attach: function (context) {
      once('ajax-placeholder', '.js-ajax-placeholder', context).forEach(function (ajaxPlaceholder) {
        let $element = $(ajaxPlaceholder);
        let endpoint = Drupal.url('ajax/placeholder/' + $element.data('hash'));
        Drupal.ajax({ url: endpoint }).execute();
      });
    }
  };

})(jQuery, window, Drupal, drupalSettings);
